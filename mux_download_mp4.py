import os
import urllib.request

import mux_python
from decouple import config
from mux_python.rest import ApiException
from pytz import timezone

# Authentication Setup
configuration = mux_python.Configuration()
configuration.username = config('MUX_USERNAME')
configuration.password = config('MUX_PASSWORD')

# API Client Initialization
assets_api = mux_python.AssetsApi(
    mux_python.ApiClient(configuration))

FOSS4G_TIMEZONE = timezone('America/Argentina/Buenos_Aires')
PATH = "/home/felipe/Downloads/FOSS4G_Videos"


def create_file_name(asset):
    """
    Function created to define file name according to the asset's recording date
    considering Buenos Aires Timezone
    :param asset: asset from asset_list()
    :return: file_name
    """
    session_date = asset.recording_times[0].started_at.astimezone(FOSS4G_TIMEZONE)
    file_name = session_date.strftime("session_date_%Y_%m_%d_time_%H_%M:%S.mp4")
    return file_name


def mux_enable_mp4_master(asset, file_name):
    """
    Function called to enable MP4 master access
    for an specific video mux asset

    :param asset: asset from asset_list
    :param file_name: string from create_file_name
    :return: None
    """
    # enabling master asset file so we can get file with higher resolution
    update_asset_master_access_request = mux_python.UpdateAssetMasterAccessRequest(master_access="temporary")
    assets_api.update_asset_master_access(
        asset.id,
        update_asset_master_access_request)

    # enabling mp4 so we can get file name
    # update_asset_mp4_support_request = mux_python.UpdateAssetMP4SupportRequest(mp4_support="standard")
    # assets_api.update_asset_mp4_support(
    #     asset.id,
    #     update_asset_mp4_support_request)
    print(
        f"Added MP4 support: {file_name}")


def manage_mux_asset():
    """
    Main function called to access mux asset and iterate over to:
    * Create file_name
    * Enable asset master access, if is the case;
    * Download asset in mp4

    :return: None
    """
    try:
        list_assets_response = assets_api.list_assets(limit=300)
        print(f'found {len(list_assets_response.data)} assets')
        for asset in list_assets_response.data:
            file_name = create_file_name(asset)
            if not asset.master:
                mux_enable_mp4_master(asset, file_name)
            elif asset.master.status == 'ready':
                if os.path.isfile(
                        os.path.join(PATH, file_name)):
                    print(f"file {file_name} already exists")
                else:
                    print(f'Downloading {file_name}')
                    asset_url = asset.master.url
                    urllib.request.urlretrieve(
                        asset_url,
                        os.path.join(PATH, file_name))

    except ApiException as e:
        print("Exception when calling AssetsApi->list_assets: %s\n" % e)


if __name__ == "__main__":
    manage_mux_asset()
