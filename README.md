# Mux downloader

script creado para descargar video almacenados en MUX.
Para usar:
crear un fichero `.env` y rellenarlo con info de usuario y la clave de acceso. [Ejemplo](./env-example).  

:warning: La clave y usuario deben tener el parámetro `write` habilitado.

## Ejecutando

`python3 mux_download_mp4.py`

El script ejecuta la función `manage_mux_asset()` que accede (con el [`list_assets()`](https://github.com/muxinc/mux-python); :warning: tenemos el limit de esta finción con 900 assets. ) y recorre por cada video (asset) en MUX. Si la versión MP4 master (mejor definición) no está habilitada, ejecuta la función `mux_enable_mp4_master()` que se la habilita de forma **temporaria**. Como eso suele llevar un tiempo, se debe volver a ejecutar la funcion `manage_mux_asset()` (en reaidad, el script) para que al recorrer por cada video, el mismo sea descargado.  

:warning: El nombre de los archivos están considerando fecha y hora de incicio del stream. Si se está intentando descargar un video que no es de stream habrá un error. Dichas fechas y hora están en formato *timezone* de Argentina.  

Instalar [`python-decouple`](https://pypi.org/project/python-decouple/),  [`mux-python`](https://pypi.org/project/mux-python/) y [`pytz`](https://pypi.org/project/pytz/).
